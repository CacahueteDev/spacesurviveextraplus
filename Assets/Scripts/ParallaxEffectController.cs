using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParallaxEffectController : MonoBehaviour
{
    [SerializeField] private Transform parallax;
    [SerializeField] private float yMin;
    [SerializeField] private float yMax;
    [SerializeField] private float timeToScroll;

    private float timer = 0f;
    
    void Start()
    {
        
    }

    void Update()
    {
        timer += Time.deltaTime;

        Vector3 pos = parallax.position;
        pos.y = Mathf.Lerp(yMin, yMax, Easing.EaseOutCirc(timer / timeToScroll));
        parallax.position = pos;
    }
}
