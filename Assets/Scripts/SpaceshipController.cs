using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpaceshipController : MonoBehaviour
{
    public static SpaceshipController Instance { get; private set; }
    
    [SerializeField] private float speed;
    [SerializeField] private Rigidbody2D rigidbody;
    [SerializeField] private TrailRenderer blasterTrail;
    
    public bool IsBlaster { get; private set; }

    private void Awake()
    {
        Instance = this;
    }

    void Start()
    {
        
    }

    void Update()
    {
        Vector2 velocity = new(0, 0);
        float torque = 0f;

        if (Input.GetKey(KeyCode.Q) || Input.GetKey(KeyCode.A)) torque = 1;
        else if (Input.GetKey(KeyCode.D)) torque = -1;
        if (Input.GetKey(KeyCode.Z) || Input.GetKey(KeyCode.W)) velocity += (Vector2)transform.up;
        else if (Input.GetKey(KeyCode.S)) velocity -= (Vector2)transform.up;

        rigidbody.AddForce(velocity * (speed * (IsBlaster ? 2f : 1f)));
        rigidbody.AddTorque(torque / 10f * (speed * (IsBlaster ? 2f : 1f)));
        
        SetBlaster(Input.GetKey(KeyCode.LeftShift));
    }

    void SetBlaster(bool isBlaster)
    {
        if (IsBlaster == isBlaster) return;
        
        IsBlaster = isBlaster;
        blasterTrail.enabled = isBlaster;
    }
}
