﻿using System;
using UnityEngine;

public static class Easing
{
    public static float EaseOutCirc(float x)
    {
        return Mathf.Sqrt(1 - Mathf.Pow(x - 1, 2));
    }
}