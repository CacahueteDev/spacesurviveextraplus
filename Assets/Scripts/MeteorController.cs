using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class MeteorController : MonoBehaviour
{
    [SerializeField] private Vector3 rotationAxis;
    [SerializeField] private TrailRenderer trail;
    [SerializeField] private GameObject[] ores;
    
    public MeteorOreType Ore { get; private set; }
    
    private float rotationMultiplier = 1f;
    
    void Start()
    {
        float scaleMultiplier = Random.Range(0.75f, 1.25f);
        transform.localScale *= scaleMultiplier;
        trail.widthMultiplier = scaleMultiplier;
        rotationMultiplier = Random.Range(0.5f, 1.5f);

        if (Random.Range(0, 10) != 0)
        {
            Ore = MeteorOreType.None;
            return;
        }
        
        Ore = (MeteorOreType) Random.Range(1, Enum.GetNames(typeof(MeteorOreType)).Length);
        for (int i = 0; i < ores.Length; i++) ores[i].SetActive(i == (int)Ore);
    }
    
    void Update()
    {
        transform.Rotate(rotationAxis * (Time.deltaTime * rotationMultiplier));
        
        if (transform.position.y < MeteorSpawner.Instance.DestroyPosition.y)
            Destroy(gameObject);
    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        if (!other.gameObject.GetComponent<SpaceshipController>()) return;
        
        if (Ore == MeteorOreType.None) GameOverController.Instance.TriggerGameOver();
        Destroy(gameObject);
    }
}

public enum MeteorOreType
{
    None,
    Red,
    Green,
    Blue
}