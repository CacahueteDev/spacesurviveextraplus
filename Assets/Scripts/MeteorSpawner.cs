using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class MeteorSpawner : MonoBehaviour
{
    public static MeteorSpawner Instance { get; private set; }
    
    [SerializeField] private Transform spawnMinPos;
    [SerializeField] private Transform spawnMaxPos;
    [SerializeField] private Transform spawnDestroyPos;
    [SerializeField] private GameObject meteorPrefab;
    [SerializeField] private float waitDuration;

    public Vector3 DestroyPosition => spawnDestroyPos.position;

    private void Awake()
    {
        Instance = this;
    }

    void Start()
    {
        StartCoroutine(SpawnMeteorCoroutine());
    }

    void Update()
    {
        
    }

    IEnumerator SpawnMeteorCoroutine()
    {
        while (!GameOverController.Instance.IsGameOver)
        {
            SpawnMeteor();

            yield return new WaitForSeconds(Random.Range(waitDuration - 0.5f, waitDuration + 0.5f));
        }
    }

    void SpawnMeteor()
    {
        Vector2 pos = new(Mathf.Lerp(spawnMinPos.position.x, spawnMaxPos.position.x, Random.Range(0f, 1f)),
            spawnMinPos.position.y);

        Instantiate(meteorPrefab, pos, Quaternion.identity);
    }
}
