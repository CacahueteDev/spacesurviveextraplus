using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameOverController : MonoBehaviour
{
    public static GameOverController Instance { get; private set; }
    
    [SerializeField] private GameObject overlay;
    [SerializeField] private Button retryButton;
    [SerializeField] private Button quitButton;
    [SerializeField] private TextMeshProUGUI finalTimeText;
    
    public bool IsGameOver { get; private set; }

    private void Awake()
    {
        Instance = this;
    }

    void Start()
    {
        retryButton.onClick.AddListener(Retry);
        quitButton.onClick.AddListener(Quit);
    }

    private void Update()
    {
        if (!IsGameOver) return;
        
        if (Input.GetKeyDown(KeyCode.Space)) Retry();
    }

    void SetOverlay(bool show) => overlay.SetActive(show);

    public void TriggerGameOver()
    {
        if (IsGameOver) return;
        IsGameOver = true;

        finalTimeText.text = TimerController.Instance.GetTimeText();
        SetOverlay(true);
        Destroy(SpaceshipController.Instance.gameObject);
        foreach (MeteorController meteor in FindObjectsOfType<MeteorController>()) Destroy(meteor.gameObject);
    }

    private void Retry()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    private void Quit()
    {
        SceneManager.LoadScene("MenuScene");
    }
}
