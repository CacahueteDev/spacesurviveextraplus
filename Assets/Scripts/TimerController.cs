using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class TimerController : MonoBehaviour
{
    public static TimerController Instance { get; private set; }
    
    [SerializeField] private TextMeshProUGUI timerText;

    private float _timer = 0f;
    private int _lastRoundedTimer = 0;

    private void Awake()
    {
        Instance = this;
    }

    void Start()
    {
        
    }

    void Update()
    {
        if (GameOverController.Instance.IsGameOver) return;

        if (Mathf.RoundToInt(_timer) != _lastRoundedTimer)
        {
            _lastRoundedTimer = Mathf.RoundToInt(_timer);
            UpdateTimerText();
        }
        
        _timer += Time.deltaTime;
    }

    public string GetTimeText()
    {
        int minutes = Mathf.RoundToInt(_timer / 60f);
        int seconds = Mathf.RoundToInt(_timer % 60f);

        return $"{minutes:00}:{seconds:00}";
    }

    void UpdateTimerText()
    {
        timerText.text = GetTimeText();
    }
}
